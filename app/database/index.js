const firebase = require('firebase');

const firebaseConfig = {
  apiKey: 'AIzaSyAEs2kRAfseFloDYafISQ0gLvSMnOErid8',
  authDomain: 'rasserver-c0ad5.firebaseapp.com',
  databaseURL: 'https://rasserver-c0ad5.firebaseio.com',
  projectId: 'rasserver-c0ad5',
  storageBucket: 'rasserver-c0ad5.appspot.com',
  messagingSenderId: '259071705961',
  appId: '1:259071705961:web:816dee671f2f2ead46b3f8',
  measurementId: 'G-8PG80JGCEX'
};
firebase.initializeApp(firebaseConfig);
const database = firebase.database();
module.exports = database;
