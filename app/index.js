/* eslint no-console: 0 */

const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const hapiAuthJWT = require('hapi-auth-jwt2');
const Mrhorse = require('mrhorse');
const routes = require('./main/routes');

require('dotenv').config();

const server = new Hapi.Server({
  host: process.env.APP_HOST,
  port: process.env.APP_PORT,
  routes: {
    cors: true,
    validate: {
      failAction: async (request, h, err) => {
        if (process.env.NODE_ENV === 'production') {
          throw err;
        } else {
          console.error(err);
          throw err;
        }
      }
    }
  }
});

const validateUser = decoded => {
  if (decoded && decoded.id) {
    return {
      isValid: true
    };
  }

  return {
    isValid: false
  };
};

const apiVersionOptions = {
  basePath: '/api',
  validVersions: [1, 2],
  defaultVersion: 1,
  vendorName: 'api'
};

const swaggerOptions = {
  pathPrefixSize: 3,
  host: process.env.HOST,
  basePath: apiVersionOptions.basePath,
  info: {
    title: 'RESTful API base Documentation',
    description:
      'This is a U-COM API documentation.' +
      '\n' +
      '###Basic api query use for getAll resources. Only support normal query if need complex or advanced use cases(fulltextsearch, geolocation...) contact server developers to support more.' +
      '\n' +
      '###$ Paginate with limit and offset. \nEx: ?limit=5&offset=5\n' +
      '###$ Order by fields. \n Ex: ?order=age asc,name desc' +
      '\n' +
      '###$ Select field on query. \nEx: ?fields=["age","name"]' +
      '\n' +
      '###$ Filter equal \nEx: ?filter={"name": "ucom"}' +
      '\n' +
      '###$ Filter less than \nEx: ?filter={"age": {"$lt": 40}}' +
      '\n' +
      '###$ Filter greater than \nEx: ?filter={"age": {"$gt": 20}}' +
      '\n' +
      '###$ Filter less than and equal \nEx: ?filter={"age": {"$lte": 40}}' +
      '\n' +
      '###$ Filter greater than equal \nEx: ?filter={"age": {"$gte": 20}}' +
      '\n' +
      '###$ Filter field in many choice \nEx: ?filter={"name": {"$in": ["ucom", "MMMM"]}}' +
      '\n' +
      '###$ Filter array field is subset of parent array \nEx: ?filter={"tags": {"$all": ["JAV", "Lesbian"]}}' +
      '\n' +
      '###$ Filter field by text \nEx: ?filter={"name": {"$like": "%co%"}}' +
      '\n' +
      '###$ Filter field by text and no distinction between upper and lowercase letters \nEx: ?filter={"name": {"$likeLower": "%co%"}}' +
      '\n' +
      '###$ See more: https://github.com/longnt189/objection-filter'
  },
  deReference: false
};

process.on('uncaughtException', err => {
  console.log(err, 'Uncaught exception');
  process.exit(1);
});

async function start() {
  try {
    await server.register([
      Inert,
      Vision,
      {
        plugin: HapiSwagger,
        options: swaggerOptions
      },
      hapiAuthJWT,
      {
        plugin: Mrhorse,
        options: {
          policyDirectory: `${__dirname}/policies`
        }
      }
    ]);
    server.auth.strategy('jwt', 'jwt', {
      key: process.env.JWT_SECRET,
      validate: validateUser,
      verifyOptions: {
        ignoreExpiration: true
      }
    });

    server.auth.default('jwt');
    server.route(routes);
    await server.start();
  } catch (err) {
    console.error(err);
    process.exit(1);
  }

  console.log('Server running at: ', server.info.uri);
}

start();

module.exports = server;

// function BFS(graph, root) {
//   const nodesLen = {}; // lưu khoảng cách đến đỉnh chọn
//   for (let i = 0; i < graph.length; i++) {
//     nodesLen[i] = Infinity; // khơi tạo cho không thế tiếp cận đc
//   }
//   nodesLen[root] = 0; // set cho cái root đến root = 0

//   const queue = [root]; // theo dõi các đỉnh đã tới

//   let current;
//   while (queue.length !== 0) {
//     current = queue.shift(); // lấy phần tử đầu tiên trong queue
//     const curConnected = graph[current];
//     const neighborIdx = [];
//     let idx = curConnected.indexOf(1);
//     while (idx !== -1) {
//       neighborIdx.push(idx);
//       console.log('ee', neighborIdx);

//       idx = curConnected.indexOf(1, idx + 1); // check từ vị trí index +1
//     } // từ 146 đến 154 là để lấy ra mảng các đỉnh kề root
//     for (let j = 0; j < neighborIdx.length; j++) {
//       if (nodesLen[neighborIdx[j]] === Infinity) {
//         nodesLen[neighborIdx[j]] = nodesLen[current] + 1;
//         queue.push(neighborIdx[j]);
//       }
//     } // vòng for là để ghép khoảng cách đến mỗi đỉnh tính từ root
//   }
//   return nodesLen;
// }
// const graph = [
//   [0, 1, 1, 1, 0],
//   [0, 0, 1, 0, 0],
//   [1, 1, 0, 0, 0],
//   [0, 0, 0, 1, 0],
//   [0, 1, 0, 0, 0]
// ];
// console.log(BFS(graph, 0));
