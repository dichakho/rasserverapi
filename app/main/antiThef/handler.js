const controller = require('./controller');
const validator = require('./validator');

exports.getMany = {
  description: 'Get Light list',
  notes: 'Return Light items',
  tags: ['api', 'v1'],
  handler: controller.getMany,
  auth: false,
  validate: {
    query: validator.queryParams
  }
};
exports.createOne = {
  description: 'Create a new Light',
  notes: 'Return created Light',
  tags: ['api', 'v1'],
  handler: controller.createOne,
  auth: false,
  validate: {
    payload: validator.create
  }
};
exports.updateOne = {
  description: 'Update Light',
  notes: 'Return updated Light by id',
  tags: ['api', 'v1'],
  handler: controller.updateOne,
  auth: false,
  validate: {
    params: {
      id: validator.idParam
    },
    payload: validator.update
  }
};
