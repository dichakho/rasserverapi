const Handler = require('./handler');

const Routes = [
  {
    method: 'GET',
    path: '/api/v1/antiThef',
    config: Handler.getMany
  },
  {
    method: 'POST',
    path: '/api/v1/antiThef',
    config: Handler.createOne
  },
  {
    method: 'PUT',
    path: '/api/v1/antiThef/{id}',
    config: Handler.updateOne
  }
];
module.exports = Routes;
