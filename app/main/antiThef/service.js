const database = require('../../database');

const antiThefRef = database.ref('antiThef');
exports.createOne = async body => {
  try {
    const result = antiThefRef.push(body);
    return result;
  } catch (error) {
    throw error;
  }
};

exports.updateOne = async (id, body) => {
  try {
    const query = database
      .ref('door')
      .orderByChild('id')
      .equalTo(id);
    query.once('child_added', snapshot => {
      snapshot.ref.update({
        state: body.state
      });
    });
    return query;
  } catch (error) {
    console.log(error);
    throw error;
  }
};
