const Joi = require('joi');
const { queryParams, idNumber } = require('../../utils/validatorUtils');

exports.queryParams = queryParams;

exports.idParam = idNumber()
  .required()
  .description('id is required');

exports.create = {
  id: Joi.number().required(),
  name: Joi.string().required(),
  state: Joi.number().default(0)
};

exports.update = {
  state: Joi.number().required()
};
