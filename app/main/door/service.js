const database = require('../../database');

const doorRef = database.ref('door');
exports.createOne = async body => {
  try {
    const result = doorRef.push(body);
    return result;
  } catch (error) {
    throw error;
  }
};

exports.getOne = async id => {
  try {
    const results = doorRef
      .orderByChild('id')
      .equalTo(id)
      .once('value', snapshot => Object.values(snapshot.val()));
    return results;
  } catch (error) {
    console.log(error);

    throw error;
  }
};
exports.updateOne = async (id, body) => {
  try {
    const query = database
      .ref('door')
      .orderByChild('id')
      .equalTo(id);
    query.once('child_added', snapshot => {
      snapshot.ref.update({
        state: body.state
      });
    });
    return query;
  } catch (error) {
    console.log(error);
    throw error;
  }
};
