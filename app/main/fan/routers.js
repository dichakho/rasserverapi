const Handler = require('./handler');

const Routes = [
  {
    method: 'GET',
    path: '/api/v1/fan',
    config: Handler.getMany
  },
  {
    method: 'POST',
    path: '/api/v1/fan',
    config: Handler.createOne
  },
  {
    method: 'PUT',
    path: '/api/v1/fan/{id}',
    config: Handler.updateOne
  }
];
module.exports = Routes;
