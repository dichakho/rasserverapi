const database = require('../../database');

const lightRef = database.ref('light');
exports.createOne = async body => {
  try {
    const result = lightRef.push(body);
    return result;
  } catch (error) {
    throw error;
  }
};

exports.getOne = async id => {
  try {
    const results = lightRef
      .orderByChild('id')
      .equalTo(id)
      .once('value', snapshot => Object.values(snapshot.val()));
    return results;
  } catch (error) {
    console.log(error);

    throw error;
  }
};

exports.updateOne = async (id, body) => {
  try {
    const query = database
      .ref('light')
      .orderByChild('id')
      .equalTo(id);
    query.once('child_added', snapshot => {
      snapshot.ref.update({
        state: body.state
      });
    });
    // let array = [];
    // const result = await database
    //   .ref('light')
    //   .on('value', snapshot => Object.values(snapshot.val()));
    const results = lightRef
      .orderByChild('id')
      .equalTo(id)
      .once('value', snapshot => Object.values(snapshot.val()));
    console.log('uiuo', (await results).val());
    return { results, status: true };
  } catch (error) {
    console.log(error);

    throw error;
  }
};
